// ======================================================================
// dfb01.v generated from TopDesign.cysch
// 05/24/2015 at 21:29
// This file is auto generated. ANY EDITS YOU MAKE MAY BE LOST WHEN THIS FILE IS REGENERATED!!!
// ======================================================================

/* -- WARNING: The following section of defines are deprecated and will be removed in a future release -- */
`define CYDEV_CHIP_DIE_LEOPARD 1
`define CYDEV_CHIP_REV_LEOPARD_PRODUCTION 3
`define CYDEV_CHIP_REV_LEOPARD_ES3 3
`define CYDEV_CHIP_REV_LEOPARD_ES2 1
`define CYDEV_CHIP_REV_LEOPARD_ES1 0
`define CYDEV_CHIP_DIE_PSOC4A 2
`define CYDEV_CHIP_REV_PSOC4A_PRODUCTION 17
`define CYDEV_CHIP_REV_PSOC4A_ES0 17
`define CYDEV_CHIP_DIE_PANTHER 3
`define CYDEV_CHIP_REV_PANTHER_PRODUCTION 1
`define CYDEV_CHIP_REV_PANTHER_ES1 1
`define CYDEV_CHIP_REV_PANTHER_ES0 0
`define CYDEV_CHIP_DIE_PSOC5LP 4
`define CYDEV_CHIP_REV_PSOC5LP_PRODUCTION 0
`define CYDEV_CHIP_REV_PSOC5LP_ES0 0
`define CYDEV_CHIP_DIE_EXPECT 4
`define CYDEV_CHIP_REV_EXPECT 0
`define CYDEV_CHIP_DIE_ACTUAL 4
/* -- WARNING: The previous section of defines are deprecated and will be removed in a future release -- */
`define CYDEV_CHIP_FAMILY_UNKNOWN 0
`define CYDEV_CHIP_MEMBER_UNKNOWN 0
`define CYDEV_CHIP_FAMILY_PSOC3 1
`define CYDEV_CHIP_MEMBER_3A 1
`define CYDEV_CHIP_REVISION_3A_PRODUCTION 3
`define CYDEV_CHIP_REVISION_3A_ES3 3
`define CYDEV_CHIP_REVISION_3A_ES2 1
`define CYDEV_CHIP_REVISION_3A_ES1 0
`define CYDEV_CHIP_FAMILY_PSOC4 2
`define CYDEV_CHIP_MEMBER_4A 2
`define CYDEV_CHIP_REVISION_4A_PRODUCTION 17
`define CYDEV_CHIP_REVISION_4A_ES0 17
`define CYDEV_CHIP_FAMILY_PSOC5 3
`define CYDEV_CHIP_MEMBER_5A 3
`define CYDEV_CHIP_REVISION_5A_PRODUCTION 1
`define CYDEV_CHIP_REVISION_5A_ES1 1
`define CYDEV_CHIP_REVISION_5A_ES0 0
`define CYDEV_CHIP_MEMBER_5B 4
`define CYDEV_CHIP_REVISION_5B_PRODUCTION 0
`define CYDEV_CHIP_REVISION_5B_ES0 0
`define CYDEV_CHIP_FAMILY_USED 3
`define CYDEV_CHIP_MEMBER_USED 4
`define CYDEV_CHIP_REVISION_USED 0
// Component: ZeroTerminal
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "$CYPRESS_DIR\..\psoc\content\cyprimitives\CyPrimitives.cylib\ZeroTerminal"
`include "$CYPRESS_DIR\..\psoc\content\cyprimitives\CyPrimitives.cylib\ZeroTerminal\ZeroTerminal.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\3.0\PSoC Creator\psoc\content\cyprimitives\CyPrimitives.cylib\ZeroTerminal"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\3.0\PSoC Creator\psoc\content\cyprimitives\CyPrimitives.cylib\ZeroTerminal\ZeroTerminal.v"
`endif

// DFB_v1_30(AssemblyCodeString=//\r\n// calculates the product of a matrix and a vector, with DFB.\r\n//\r\n// input: k-dimensional vector (u)\r\n// output: l-dimensional vector (v)\r\n// internal state: n-dimensional vector (x)\r\n// constants: (n+l) x (n+k)-dimensional matrix (A)\r\n//\r\n// caclulates folloing update rule:\r\n// [x(t+1); v(t+1)] = A*[x(t); u(t)]\r\n//\r\n// memory organization:\r\n// ACU RAM:\r\n//    {0, n, k, l, n+k, n+l, (n+k)*(n+l), tmp, 64, 64+n, 64+n+l, ...}\r\n//\r\n// RAM-A:\r\n//    {x[0], x[1], ..., x[n-1], u[0], ..., u[k-1]}\r\n// or {x[0], x[1], ..., x[n-1], v[0], ..., v[l-1]}\r\n//\r\n// RAM-B:\r\n//    {A[0][0], A[0][1], ..., A[0][n-1], A[1][0], ..., A[n-1][n-1]\r\n//\r\n\r\n// constants for simulation\r\n\r\n// define ACU memory content\r\narea(ACU)\r\ndw 0x0000       // [0] 0\r\ndw 0x0101       // [1] n == 1\r\ndw 0x0303       // [2] k == 3\r\ndw 0x0101       // [3] l == 1\r\ndw 0x0404       // [4] n+k == 4\r\ndw 0x0202       // [5] n+l == 2\r\ndw 0x0808       // [6] (n+k)*(n+l) == 4\r\ndw 0x4040       // [7] 64\r\ndw 0x4141       // [8] 64+n == 65\r\ndw 0x4242       // [9] 64+n+l == 66\r\ndw 0x0000       // [10] tmp1\r\ndw 0x0000       // [11] tmp2\r\n\r\n// define RAM-A content\r\narea(data_a)\r\ndw 0x800000\r\ndw 0xffff00\r\ndw 0x010000\r\ndw 0x000100\r\ndw 0x100000\r\ndw 0x010000\r\ndw 0x000100\r\ndw 0x000000\r\n\r\n// define RAM-B content\r\narea(data_b)\r\ndw 0x800000\r\ndw 0x400000\r\ndw 0x200000\r\ndw 0x100000\r\ndw 0x001000\r\ndw 0xffff00\r\ndw 0x010000\r\ndw 0x000100\r\ndw 0x100000\r\ndw 0x010000\r\ndw 0x000100\r\ndw 0x000000\r\n\r\n\r\n// initialize, just jump to the wait loop.\r\ninit:\r\nacu(hold, loadm) addr(6) dmux(srm, srm) alu(hold) mac(hold)                         // load (n+k)*(n+l) into regm of b.\r\n//acu(loadf, hold) addr(9) dmux(srm, srm) alu(hold) mac(hold)                         // load 64 to regf of a.\r\nacu(setmod, setmod) dmux(srm, srm) alu(hold) mac(hold) jmp(eob, load_init)          // jump to wait loop.\r\n\r\n// `n'(at ACU[1]) -> reg, `n+k'(at ACU[4]) -> mreg\r\nload_init:\r\nacu(loadm, hold) addr(4) dmux(ba, srm) alu(hold) mac(hold)                         // `n+k' -> mreg\r\nacu(read, hold) addr(1) dmux(ba, srm) alu(setsem, 001) mac(hold) jmp(eob, load_dummy)      // `n' -> reg \r\n\r\nload_outer_loop:\r\nacu(incr, hold) addr(1) dmux(ba, srm) alu(setsem, 001) mac(hold) jmp(acuaeq, mul_init)\r\nload_dummy:\r\nacu(hold, hold) addr(1) dmux(ba, srm) alu(hold) mac(hold) jmp(eob, load_inner_loop)\r\nload_inner_loop:\r\nacu(hold, hold) addr(1) dmux(ba, srm) alu(hold) mac(hold) write(da) jmpl(in1, load_outer_loop)\r\n\r\nmul_init:\r\nacu(read, hold) addr(7) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(write, hold) addr(10) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(clear, clear) dmux(srm, srm) alu(set0) mac(clra) jmp(eob, mul_loop)\r\n\r\nmul_loop:\r\nacu(incr, incr) dmux(srm, srm) alu(seta) mac(macc) jmpl(acuaeq, mul_finish)\r\n\r\nmul_finish:\r\nacu(read, hold) addr(10) dmux(srm, srm) alu(hold) mac(clra)\r\nacu(hold, hold) dmux(srm, srm) alu(hold) mac(hold) write(da)\r\nacu(incr, hold) dmux(srm, srm) alu(hold) mac(hold) jmp(acubeq, copy_init)\r\nmul_finish_dummy:\r\nacu(write, hold) addr(10) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(clear, hold) dmux(srm, srm) alu(set0) mac(clra) jmp(eob, mul_loop)\r\n\r\nmul_dummy:\r\nacu(hold, hold) dmux(srm, srm) alu(hold) mac(hold) jmp(eob, load_init)\r\n\r\ncopy_init:\r\nacu(read, hold) addr(7) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(write, hold) addr(10) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(clear, hold) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(write, hold) addr(11) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(loadm, hold) addr(1) dmux(srm, srm) alu(hold) mac(hold) jmp(eob, copy_loop)\r\n\r\ncopy_loop:\r\nacu(read, hold) addr(10) dmux(sra, srm) alu(seta) mac(hold)\r\nacu(incr, hold) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(write, hold) addr(10) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(read, hold) addr(11) dmux(srm, srm) alu(hold) mac(hold) write(da)\r\nacu(incr, hold) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(write, hold) addr(11) dmux(srm, srm) alu(hold) mac(hold) jmpl(acuaeq, write_init)\r\n\r\nwrite_init:\r\nacu(loadm, hold) addr(9) dmux(srm, srm) alu(hold) mac(hold) jmp(eob, write_loop)\r\n\r\nwrite_loop:\r\nacu(read, hold) addr(10) dmux(sra, srm) alu(seta) mac(hold)\r\nacu(incr, hold) addr(1) dmux(srm, srm) alu(hold) mac(hold)\r\nacu(hold, hold) dmux(srm, srm) alu(hold) mac(hold) write(abus) jmpl(acuaeq, write_dummy)\r\n\r\nwrite_dummy:\r\nacu(hold, hold) dmux(srm, srm) alu(hold) mac(hold) jmp(eob, load_init)\r\n, Bus1_data=1\r\n2\r\n0x100000\r\n0x010000\r\n0x001000\r\n10\r\n11\r\n12\r\n0x100000\r\n0x010000\r\n0x001000\r\n0x100000\r\n0x010000\r\n0x001000\r\n0x100000\r\n0x010000\r\n0x001000\r\n0x100000\r\n0x010000\r\n0x001000\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0, Bus2_data=0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0, CustomizerLayout=CYXML_v_1_0<?xml version="1.0" encoding="utf-16"?>\r\n<!--\r\n\r\n  Actipro UIStudio Dock Controls\r\n  Copyright (c) 2001-2006 Actipro Software LLC.  All rights reserved.\r\n  http://www.ActiproSoftware.com/Products/DotNet/\r\n\r\n-->\r\n<ToolWindowLayout Version="1.0">\r\n  <LayoutData>\r\n    <HostContainerControl>\r\n      <ToolWindowContainer Dock="Right" Size="302, 397" SelectedToolWindowGuid="cef5a82e-7dcf-4b8f-8dab-1455a0c935b2">\r\n        <ToolWindow Key="Bus1Data" Guid="cef5a82e-7dcf-4b8f-8dab-1455a0c935b2" DockedSize="298, 200" ImageIndex="-1" Text="Bus1">\r\n          <AutoHideStateInfo RootDock="Right" />\r\n          <DockedInsideHostStateInfo RootDock="Right" IsAttached="True">\r\n            <AttachedToBefore Guid="ef672197-9e58-45ec-8f6f-3bbd4d104368" />\r\n            <AttachedToBefore Guid="68a8dd28-2d5a-4600-a4bd-336d8eb0bcfe" />\r\n            <DockedBy Guid="f5c3c173-522a-453c-af36-6ef9112fe164" RootDock="Bottom" DockOperationType="TopOuter" IsTopMost="True" />\r\n            <DockedBy Guid="8733fb18-371e-4cd5-81b1-1a7bb6e79a6b" RootDock="Bottom" DockOperationType="TopOuter" IsTopMost="True" />\r\n          </DockedInsideHostStateInfo>\r\n          <DockedOutsideHostStateInfo IsAttached="False" />\r\n        </ToolWindow>\r\n        <ToolWindow Key="Bus2Data" Guid="ef672197-9e58-45ec-8f6f-3bbd4d104368" DockedSize="298, 200" ImageIndex="-1" Text="Bus2">\r\n          <AutoHideStateInfo RootDock="Right" />\r\n          <DockedInsideHostStateInfo RootDock="Right" IsAttached="True">\r\n            <AttachedToBefore Guid="68a8dd28-2d5a-4600-a4bd-336d8eb0bcfe" />\r\n            <AttachedToAfter Guid="cef5a82e-7dcf-4b8f-8dab-1455a0c935b2" />\r\n            <DockedBy Guid="f5c3c173-522a-453c-af36-6ef9112fe164" RootDock="Bottom" DockOperationType="TopOuter" IsTopMost="True" />\r\n            <DockedBy Guid="8733fb18-371e-4cd5-81b1-1a7bb6e79a6b" RootDock="Bottom" DockOperationType="TopOuter" IsTopMost="True" />\r\n          </DockedInsideHostStateInfo>\r\n          <DockedOutsideHostStateInfo IsAttached="False" />\r\n        </ToolWindow>\r\n        <ToolWindow Key="SimulationProperties" Guid="68a8dd28-2d5a-4600-a4bd-336d8eb0bcfe" DockedSize="298, 200" ImageIndex="-1" Text="Simulation Properties">\r\n          <AutoHideStateInfo RootDock="Right" />\r\n          <DockedInsideHostStateInfo RootDock="Right" IsAttached="True">\r\n            <AttachedToAfter Guid="cef5a82e-7dcf-4b8f-8dab-1455a0c935b2" />\r\n            <AttachedToAfter Guid="ef672197-9e58-45ec-8f6f-3bbd4d104368" />\r\n            <DockedBy Guid="f5c3c173-522a-453c-af36-6ef9112fe164" RootDock="Bottom" DockOperationType="TopOuter" IsTopMost="True" />\r\n            <DockedBy Guid="8733fb18-371e-4cd5-81b1-1a7bb6e79a6b" RootDock="Bottom" DockOperationType="TopOuter" IsTopMost="True" />\r\n          </DockedInsideHostStateInfo>\r\n          <DockedOutsideHostStateInfo IsAttached="False" />\r\n        </ToolWindow>\r\n      </ToolWindowContainer>\r\n      <ToolWindowContainer Dock="Bottom" Size="1363, 281" SelectedToolWindowGuid="f5c3c173-522a-453c-af36-6ef9112fe164">\r\n        <ToolWindow Key="Output" Guid="f5c3c173-522a-453c-af36-6ef9112fe164" DockedSize="200, 414" ImageIndex="-1" Text="Output">\r\n          <AutoHideStateInfo RootDock="Bottom" />\r\n          <DockedInsideHostStateInfo RootDock="Bottom" IsAttached="True">\r\n            <AttachedToBefore Guid="8733fb18-371e-4cd5-81b1-1a7bb6e79a6b" />\r\n          </DockedInsideHostStateInfo>\r\n          <DockedOutsideHostStateInfo IsAttached="False" />\r\n        </ToolWindow>\r\n        <ToolWindow Key="ErrorList" Guid="8733fb18-371e-4cd5-81b1-1a7bb6e79a6b" DockedSize="200, 414" ImageIndex="-1" Text="Error List: 0 Errors    2 Warnings    0 Notes">\r\n          <AutoHideStateInfo RootDock="Bottom" />\r\n          <DockedInsideHostStateInfo RootDock="Bottom" IsAttached="True">\r\n            <AttachedToAfter Guid="f5c3c173-522a-453c-af36-6ef9112fe164" />\r\n          </DockedInsideHostStateInfo>\r\n          <DockedOutsideHostStateInfo IsAttached="False" />\r\n        </ToolWindow>\r\n      </ToolWindowContainer>\r\n    </HostContainerControl>\r\n    <AutoHide Dock="Left" />\r\n    <AutoHide Dock="Right" />\r\n    <AutoHide Dock="Top" />\r\n    <AutoHide Dock="Bottom" />\r\n    <TabbedDocuments Orientation="Horizontal" />\r\n    <FloatingContainers />\r\n    <Hidden />\r\n  </LayoutData>\r\n  <CustomData />\r\n</ToolWindowLayout>, InitialDMAMode=9, InitialInterruptMode=0, InitialOutput1Source=0, InitialOutput2Source=0, OptimizeAssembly=true, ShowDMA1=true, ShowDMA2=true, ShowIn1=false, ShowIn2=false, ShowOut1=false, ShowOut2=false, CY_COMPONENT_NAME=DFB_v1_30, CY_CONTROL_FILE=<:default:>, CY_FITTER_NAME=DFB_1, CY_INSTANCE_SHORT_NAME=DFB_1, CY_MAJOR_VERSION=1, CY_MINOR_VERSION=30, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=cydsfit No Version Information Found, INSTANCE_NAME=DFB_1, )
module DFB_v1_30_0 (
    out_2,
    out_1,
    in_2,
    in_1,
    interrupt,
    dma_req_a,
    dma_req_b);
    output      out_2;
    output      out_1;
    input       in_2;
    input       in_1;
    output      interrupt;
    output      dma_req_a;
    output      dma_req_b;


          wire  Net_38;

    cy_psoc3_dfb_v1_0 DFB (
        .in_1(in_1),
        .out_1(out_1),
        .out_2(out_2),
        .interrupt(interrupt),
        .in_2(in_2),
        .clock(Net_38),
        .dmareq_1(dma_req_a),
        .dmareq_2(dma_req_b));

    ZeroTerminal ZeroTerminal_1 (
        .z(Net_38));



endmodule

// top
module top ;

          wire  Net_7;
          wire  Net_6;
          wire  Net_5;
          wire  Net_4;
          wire  Net_3;
          wire  Net_2;
          wire  Net_1;

    DFB_v1_30_0 DFB_1 (
        .interrupt(Net_1),
        .in_1(1'b0),
        .in_2(1'b0),
        .out_1(Net_4),
        .out_2(Net_5),
        .dma_req_a(Net_6),
        .dma_req_b(Net_7));



endmodule

