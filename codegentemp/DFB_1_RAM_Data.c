#include "cytypes.h"
#include "DFB_1.h"

const uint32 CYCODE DFB_1_cstoreA[] = 
{
    0xFE8000ECu,      0xFE801321u,      0x1E980203u,      0x1E800003u,  
    0xFF100221u,      0xFE80060Eu,      0xFE800814u,      0xFE801600u,  
    0xFE800816u,      0xFE800E03u,      0xFE800E13u,      0xFE800001u,  

};

const uint32 CYCODE DFB_1_cstoreB[] = 
{
    0x1E800E08u,      0x1E980603u,      0x1E808003u,      0xFE80060Eu,  
    0xFE800814u,      0xFD001761u,      0xFD800614u,      0xFE808000u,  
    0xFE800201u,      0xFE800814u,      0xFD001601u,      0x7E100614u,  
    0xFE800200u,      0xFE800814u,      0xFE808616u,      0xFE800200u,  
    0xFE800817u,      0x7E100614u,      0xFE800202u,      0xFE802001u,  

};

const uint32 CYCODE DFB_1_data_a[] = 
{
    0x00800000u,      0x00FFFF00u,      0x00010000u,      0x00000100u,  
    0x00100000u,      0x00010000u,      0x00000100u,  
};

const uint32 CYCODE DFB_1_data_b[] = 
{
    0x00800000u,      0x00400000u,      0x00200000u,      0x00100000u,  
    0x00001000u,      0x00FFFF00u,      0x00010000u,      0x00000100u,  
    0x00100000u,      0x00010000u,      0x00000100u,  
};

const uint32 CYCODE DFB_1_acu[] = 
{
    0x00000000u,      0x00000081u,      0x00000183u,      0x00000081u,  
    0x00000204u,      0x00000102u,      0x00000408u,      0x00002040u,  
    0x000020C1u,      0x00002142u,  
};

const uint32 CYCODE DFB_1_cfsm[] = 
{
    0x0101F821u,      0x0107F843u,      0x1007F865u,      0x0105F884u,  
    0x40841042u,      0x0109F8C6u,      0x108C2087u,      0x200BF90Au,  
    0x0109F926u,      0x0105F941u,      0x0117F96Bu,      0x1094816Cu,  
    0x0123F9ADu,      0x10969A2Eu,      0x0101F801u,      0x00000000u,  

};

